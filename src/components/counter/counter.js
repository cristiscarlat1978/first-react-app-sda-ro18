import { useState, useEffect } from 'react';
import './counter.css';


const Counter = (props) => {
    const [count, setCount] = useState(props.initValue || 0);

    return (
        <div>
            <div className="counter-display">{count}</div>
            <div className="counter-controls">
                <button onClick={() => setCount(count-1)}>-</button>
                <button onClick={() => setCount(count+1)}>+</button>
            </div>
        </div>
    )
}

export default Counter;
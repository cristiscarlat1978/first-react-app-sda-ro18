

const Article = (props) => {

    return (
        <div>
            <h3>{props.title}</h3>
            <p>
                {props.content}
                <img src={props.imgPath} alt="..." style={{width: '320px', height: 320, objectFit: 'cover', float: 'left'}}/>
            </p>
        </div>
    )
}

export default Article;
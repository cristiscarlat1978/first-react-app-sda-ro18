import { useEffect, useState } from "react"


const Timer = (props) => {
    const [seconds, setSeconds] = useState(props.initialValue);

    useEffect(() => {
        const tick = setInterval(() => {
            setSeconds(prevState => prevState + 1)
        }, 1000);
        //clearInterval se executa inainte de demontarea componentei
        return () => clearInterval(tick);
    }, [])

    return(
        <div>
            {seconds}
        </div>
    )
}

export default Timer;
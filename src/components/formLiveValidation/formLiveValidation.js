import { useState } from "react";
import "../form/form.css";

const FormLiveValidation = (props) => {
    const [error, setError] = useState({
        name: null,
        email: null,
        pass: null
    });
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const handleNameOnChange = (e) => {
        const inputValue = e.target.value;
        const inputName = e.target.name;
 
        const obj = {
            name: error.name,
            email: error.email,
            pass: error.pass
        }
        if(inputName === "name"){
            const reg = /^[0-9]+$/g;

            if (reg.test(inputValue)){
                obj.name = "Digits are not allowed!"
            }
            setName(inputValue);
        }

        if(inputName === "email"){
            if (!inputValue.includes('@')){
                obj.email = "Email should contain @ character."
            }
            setEmail(inputValue)
        }
        
        setError(obj);
    }

    return (
        <div className="form-container">
            <form>
                <div className="form-item">
                    <label htmlFor="name">Name</label>
                    <div>
                        <input id="name" type="text" name="name" value={name} onChange={handleNameOnChange} style={{backgroundColor: error.name ? "red" : "transparent", color: error.name ? "white" : "black"}}/>
                        {error.name ? <div style={{ color: 'red' }}>{error.name}</div> : null}
                    </div>
                </div>
                <div className="form-item">
                    <label htmlFor="email">Email</label>
                    <div>
                        <input id="email" type="text" name="email" value={email} onChange={handleNameOnChange} />
                        {error.email && <div style={{ color: 'red' }}>{error.email}</div>}
                    </div>
                </div>
                <div className="form-item">
                    <label htmlFor="pass">Password</label>
                    <div>
                        <input id="pass" type="password" />
                        {error.pass && <div style={{ color: 'red' }}>{error.pass}</div>}
                    </div>
                </div>
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}

export default FormLiveValidation;
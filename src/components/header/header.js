import React, { useState, useEffect } from 'react';
import IconButton from '../iconButton/iconButton';
import { getArticlesImg } from '../../data/data';
import './header.css';

const Header = () => {
    const [list, setList] = useState([]);
    const [pulse, setPulse] = useState(0);
    const [showHeader, setShowHeader] = useState(false);

    async function getData() {
        const res = await getArticlesImg();
        console.log(res)
        setList(res)
    }

    useEffect(() => {
        console.log("Headerul s-a montat")
        getData();
        const id = setInterval(() => {
            setPulse(prev => prev + 1)
        }, 10)
        //clearInterval se va executa inaintea demontarii componentei
        return () => clearInterval(id)
    }, [])

    useEffect(() => {
        console.log(pulse)
    }, [list])

    return (
        <>
            {showHeader ? <div className="header-container">
                <div>
                    <div className="header-brand">JS-RO18</div>
                    <IconButton />
                </div>
                <div>
                    {list.map(l => <img src={l} width={120} />)}
                </div>
                <div className="header-links">
                    <ul>
                        <li>Home</li>
                        <li>About</li>
                    </ul>
                </div>
            </div> : null}
            <button onClick={() => setShowHeader(prev => !prev)}>Show Header</button>
        </>
    )
}

export default Header;
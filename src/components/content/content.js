import { useState, useEffect } from 'react';
import { articles, getArticles } from '../../data/data';

const Content = () => {
    const [list, setList] = useState([]);

    async function getData() {
        const res = await getArticles();
        console.log(res)
        setList(res)
    }
    
    //cb se executa doar la montarea componentei -> array ul de dependinte este gol
    useEffect(() => {
        console.log("componenta s-a montat")
        getData()
    }, [])

    //cb se executa la fiecare rerandare -> array ul de dependinte lipseste
    // useEffect(() => {
    //     console.log("componenta s-a rerandat")
    //     getData()
    // })

    //cb se executa la fiecare schimbare a dependintei "list" -> array ul de dependinte populat
    // useEffect(() => {
    //     console.log("list s-a schimbat")
    //     getData()
    // }, [list])

    return(
        <div>
            <ul>
                {list.map((article, index) => <li key={article.title + "-" + index}>{article.title}</li>)}
            </ul>
        </div>
    )
}

export default Content;
import { useState } from "react";
import "./form.css";

const Form = (props) => {
    const [error, setError] = useState({
        name: null,
        email: null,
        pass: null
    });

    const handleSubmit = (e) => {
        console.dir(e)
        e.preventDefault();
        const name = e.target[0].value;
        const email = e.target[1].value;
        const pass = e.target[2].value;
        const temp = {
            name: null,
            email: null,
            pass: null
        };
        if (name === "") {
            temp.name = "Name cannot be empty.";
        }
        if (email === "") {
            temp.email = "Email cannot be empty.";
        }
        if (pass === "") {
            temp.pass = "Pass cannot be empty.";
        }
        if (pass.length > 0 && pass.length < 8) {
            temp.pass = "8 characters length";
        }
        setError(temp);
        props.onChange({name, email, pass})
    }

    return (
        <div className="form-container">
            <form onSubmit={handleSubmit}>
                <div className="form-item">
                    <label htmlFor="name">Name</label>
                    <div>
                        <input id="name" type="text" />
                        {error.name ? <div style={{ color: 'red' }}>{error.name}</div> : null}
                    </div>
                </div>
                <div className="form-item">
                    <label htmlFor="email">Email</label>
                    <div>
                        <input id="email" type="email" />
                        {error.email && <div style={{ color: 'red' }}>{error.email}</div>}
                    </div>
                </div>
                <div className="form-item">
                    <label htmlFor="pass">Password</label>
                    <div>
                        <input id="pass" type="password" />
                        {error.pass && <div style={{ color: 'red' }}>{error.pass}</div>}
                    </div>
                </div>
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}

export default Form;